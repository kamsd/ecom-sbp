import express from 'express';
const app = express();

import Tinkoff from './vendors/tinkoff.js'

import bodyParser from 'body-parser';
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var HTTP_PORT = 8000

app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});

app.post('/api/v1/sbp', async function(request, response){
    let tinkoff = new Tinkoff();
    if (!request.body.url) {
        response.send({error: "Link missing"});  
    }
    let result = null
    if (request.body.vendor) {
        if (request.body.vendor == "tinkoff") {
            console.log(request.body);   
            result = await tinkoff.getSBP(request.body.url)
        } else {
            response.send({error: "Unsupported vendor"});  
        }
        response.send(result);
    } else {
        response.send({error: "Not selected vendor"});  
    }
    
  });

app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});