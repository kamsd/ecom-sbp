import puppeteer from 'puppeteer';
import { promises as fs } from 'fs'
import pkg from 'convert-svg-to-png'
const { convertFile }  = pkg;
import QrCode from 'qrcode-reader';
import Jimp from "jimp";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

export default class Tinkoff{

  constructor(){
  }

  async getSBP(link) {
      let startDate = Date.now();
      console.log("Start");
      const browser = await puppeteer.launch({args: [
        '--disable-setuid-sandbox',
        '--disable-dev-shm-usage',
        '--disable-accelerated-2d-canvas',
        '--no-first-run',
        '--disable-gpu'
      ]});
      
      const page = await browser.newPage();
      await page.setViewport({ width: 400, height: 10 });
      await page.goto(link);

      try {
        await page.waitForSelector('[automation-id=payment-button__QR]', {timeout: 3000 });
      } catch (e) {
        console.log("Bad link or not found SBP QR")
        return {error: "Bad link or not found SBP QR", link: link}
      }

      console.log("Page is loaded")

      await page.evaluate(()=>{
        document.querySelector('[automation-id=payment-button__QR]').click();
      })

      await page.waitForTimeout(350);

      let element = null;

      try {
        element = await page.waitForSelector('[automation-id=custom-dialog__pop-up] > pf-fps > tui-svg > div', {timeout: 3000 });
      } catch (e) {
        console.log("Not found QR content")
        return {error: "Not found QR content"}
      }

      console.log("QR is loaded")

      let svg_raw = await page.evaluate(el => el.innerHTML, element)
      let svg_string = svg_raw.replaceAll('"0.08"', '"1.00"')
    
      await browser.close();

      let filename_svg = "tmp/" + this.makeid();
      
      await fs.writeFile(filename_svg + ".svg", '<?xml version="1.0" encoding="UTF-8" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">'+svg_string)

      console.log("QR was saved: " + filename_svg + ".svg");

      await convertFile(filename_svg + ".svg", {background: "#fff", scale: 1, height: 400, width: 400});

      console.log("QR was converted");

      let result_decode = false;

      let buffer = await fs.readFile(filename_svg + ".png");
      
      let image = await Jimp.read(buffer)

      let qr = new QrCode();

      let error = false;
      
      qr.callback = function(err, value) {
          if (err) {
              console.error(err);
              error = err
              console.log("Error: " + err);
              console.log(link);
              //console.log("Try again ... ");
              //return {error: err}
          } else {
            console.log("Task completed");
            console.log(value.result);
            result_decode = value.result
          }
          
      };
      
      qr.decode(image.bitmap)


      await fs.unlink(filename_svg + ".png");
      await fs.unlink(filename_svg + ".svg");

      console.log((new Date()  - startDate) / 1000);

      if (!result_decode)
        return {
          error: true
        }

      return {
        url: result_decode
      }
  }

  makeid() {
      let length = 16
      var result           = '';
      var characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * 
  charactersLength));
    }
    return result;
  }

}